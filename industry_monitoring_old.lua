-- Refiner status rename return for display purposes
screen_right.clear()

-- strip status name, so it can be show nicely on screen
function stripStatus (status)

	if status == "JAMMED_MISSING_INGREDIENT"
		then
			result = "JAMMED"
			return result
        	
	elseif status == "JAMMED_OUTPUT_FULL"
		then
 			result = "OUTPUT_FULL"
			return result
     else
		return status
	end
    
end

local names = {refiner = refiner,
    		refiner2 = refiner2,
    		glassfurnace = glassfurnace,
    		metalwork = metalwork,
    		assemblylineM = assemblylineM,
    		assemblylineL = assemblylineL,
		    assemblylineS = assemblylineS,
    		smelter1 = smelter1,
    		smelter2 = smelter2}

yPos = 0
for k,v in pairs(names) do
    
    --system.print(tostring(k)..': '..stripStatus(v.getStatus()))
    screen_right.addText(0, yPos, 5, tostring(k)..': '..stripStatus(v.getStatus()))
    yPos = yPos + 10
end